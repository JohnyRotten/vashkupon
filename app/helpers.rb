require 'mysql2'
require 'data_mapper'
require 'pony'
require 'digest'
require './app/modeles.rb'

module Helper 
	def protected!
		raise Exception.new if @user.nil?
		raise Exception.new if 0b100 & @user.rights == 0
	end

	def protected?
		begin
			protected!
			false
		rescue => e 
			true
		end
	end

	def crypt(str)
		Digest::SHA1.hexdigest str
	end

	def getComments(item_id)
		Comment.all item_id: item_id
	end

	def getLikes(item_id)
		Like.all item_id: item_id
	end

	def getReviews(item_id)
		Review.all item_id: item_id
	end

	def getSales(item_id)
		Sales.all(item_id: item_id)
	end

	def getCompanyByCID(item_id)

	end

	def getCompanyByUID(user_id)

	end

	def getUserNameById(user_id)
		User.first(id: user_id).login
	end
end