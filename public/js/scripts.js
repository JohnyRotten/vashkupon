$(function(){
	$('body').on('submit', 'form[method="put"]', function(){
		var params = {
			data: $(this).serialize(),
			url: $(this).attr('action'),
			method: 'put'
		}
		$.ajax(params);
	});

	$('body').on('click', '.delete', function(){
		var href = $(this).attr('href');
		$.ajax({ url: href, method: 'delete' }).done(function(){
			$('#main').load(document.URL + ' #main>*');
		});
		return false;
	});

	$('body').on('click', '.like', function(){
		var params = {
			item_id: $(this).data('id')
		}
		$.ajax({
			url: '/likes',
			data: params,
			method: 'post'
		}).done(function(){
			$('#main').load(document.URL + ' #main>*');
		});
		return false;
	});

	$('body').on('click', '.sale', function(){
		var params = {
			item_id: $(this).data('id')
		}
		$.ajax({
			url: '/sales',
			method: 'post',
			data: params
		}).done(function(){
			$('#main').load(document.URL + ' #main>*');
		});
		return false;
	});

	$('body').on('submit', '.comments form', function(){
		var params = {
			url: $(this).attr('action'),
			data: $(this).serialize(),
			method: ($(this).attr('method') || 'post')
		}
		$.ajax(params).done(function(){
			$('#main').load(document.URL + ' #main>*');
		});
		return false;
	});


	$('body').on('submit', '.reviews form', function(){
		var params = {
			url: $(this).attr('action'),
			data: $(this).serialize(),
			method: ($(this).attr('method') || 'post')
		}
		$.ajax(params).done(function(){
			$('#main').load(document.URL + ' #main>*');
		});
		return false;
	});
});