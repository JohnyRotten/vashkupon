require 'data_mapper'
require 'dm-migrations'
require 'dm-mysql-adapter'

# DataMapper.setup(:default, ENV['DATABASE_URL'] || 'sqlite:./db/page.db')
DataMapper.setup(:default, 'mysql://root:1234@localhost/vashkupon')

class User
	include DataMapper::Resource

	property :id,			Serial
	property :login,		String
	property :email,		String
	property :password,		String
	property :created_at,	DateTime
	property :updated_at,	DateTime
	property :rights, 		Integer
end

class Sales
	include DataMapper::Resource

	property :id,			Serial
	property :user_id,		Integer
	property :item_id, 		Integer
	property :created_at,	DateTime
end

class Comment
	include DataMapper::Resource

	property :id, 			Serial
	property :user_id,		String
	property :item_id,		String
	property :text,			String
	property :created_at,	DateTime
end

class Faq
	include DataMapper::Resource

	property :id, 			Serial
	property :question,		Text 
	property :answer, 		Text
end

class Coupon
	include DataMapper::Resource

	property :id, 			Serial
	property :name, 		String
	property :price_from, 	Float
	property :price_to, 	Float
	property :price_save, 	Float
	property :percent,		Float
	property :conds,		Text
	property :description,	Text
	property :count_sales, 	Integer
	property :date_from,	DateTime
	property :date_to,		DateTime
	property :category_id,	Integer
	property :company_id,	Integer
end

class Like
	include DataMapper::Resource

	property :id,			Serial
	property :item_id,		Integer
	property :user_id,		Integer
end

class Category
	include DataMapper::Resource

	property :id, 			Serial
	property :name,			String
	property :description,	Text
end

class Review
	include DataMapper::Resource

	property :id,			Serial
	property :user_id,		Integer
	property :item_id,		Integer
	property :created_at,	DateTime
	property :mark,			Integer
	property :text,			Text
end

class Question
	include DataMapper::Resource

	property :id, 			Serial
	property :user_id,		Integer
	property :item_id, 		Integer
	property :question,		Text
	property :created_at,	DateTime
end

class Answer
	include DataMapper::Resource

	property :id,			Serial
	property :user_id,		Integer
	property :q_id,			Integer
	property :answer,		Text
	property :created_at,	DateTime
end

class Company
	include DataMapper::Resource

	property :id, 			Serial
	property :name,			String
	property :description,	Text
	property :address,		String
	property :email,		String
end

class Filter

end

DataMapper.auto_migrate!
DataMapper.auto_upgrade!

# Create admin
User.create(
	id: 		1,
	login: 		'admin',
	email: 		'admin@voove.ru',
	password: 	Digest::SHA1.hexdigest('admin'),
	rights: 	0b111,
	created_at: Time.now,
	updated_at: Time.now
	)
User.create(
	id: 		2,
	login: 		'company',
	email: 		'company@voove.ru',
	password: 	Digest::SHA1.hexdigest('company'),
	rights: 	0b011,
	created_at: Time.now,
	updated_at: Time.now
	)
User.create(
	id: 		3,
	login: 		'user',
	email: 		'user@voove.ru',
	password: 	Digest::SHA1.hexdigest('user'),
	rights: 	0b001,
	created_at: Time.now,
	updated_at: Time.now
	)
Coupon.create(
	id: 1,
	name: 			'Coupon #1',
	price_from: 	949.99,
	price_to: 		449.99,
	price_save: 	500,
	percent: 		0.63,
	conds: 			'Condition1, cond2',
	description: 	'Very GOOD condition! Buy ME!!!',
	count_sales: 	1222,
	date_from: 		DateTime.parse('2014-10-01 12:00:00'),
	date_to: 		DateTime.parse('2014-11-02 12:00:00'),
	category_id: 	1,
	company_id: 	1
	)
Coupon.create(
	id: 2,
	name: 			'Coupon #2',
	price_from: 	949.99,
	price_to: 		449.99,
	price_save: 	500,
	percent: 		0.63,
	conds: 			'Condition1, cond2',
	description: 	'Very GOOD condition! Buy ME!!!',
	count_sales: 	122,
	date_from: 		DateTime.parse('2014-10-01 12:00:00'),
	date_to: 		DateTime.parse('2014-11-02 12:00:00'),
	category_id: 	2,
	company_id: 	1
	)
Coupon.create(
	id: 3,
	name: 			'Coupon #3',
	price_from: 	1000,
	price_to: 		320,
	price_save: 	680,
	percent: 		0.68,
	conds: 			'Condition1, cond2',
	description: 	'Very GOOD condition! Buy ME!!!',
	count_sales: 	122,
	date_from: 		DateTime.parse('2014-10-21 12:00:00'),
	date_to: 		DateTime.parse('2014-10-31 12:00:00'),
	category_id: 	2,
	company_id: 	2
	)
Faq.create(
	id: 1,
	answer: 'FAADSADADDSADDSA',
	question: 'asdadssadadASDASDASSDA')
Faq.create(
	id: 2,
	answer: 'sdadsadadFAADSADADDSADDSA',
	question: 'asdadssaASSDASDadadASDASDASSDA')
Faq.create(
	id: 3,
	answer: 'FAADSADADDSADDSAsdaSDA',
	question: 'asdadssadadASDASDASSsadaDA')
Like.create(
	id: 1,
	item_id: 1,
	user_id: 2)
Like.create(
	id: 2,
	item_id: 2,
	user_id: 2)
Like.create(
	id: 3,
	item_id: 1,
	user_id: 3)
Like.create(
	id: 4,
	item_id: 2,
	user_id: 3)
Category.create(
	id: 1,
	name: 'Test1',
	description: 'Test1')
Category.create(
	id: 2,
	name: 'Test2',
	description: 'Test2')
Category.create(
	id: 3,
	name: 'Test3',
	description: 'Test3')
Category.create(
	id: 4,
	name: 'Test4',
	description: 'Test4')
Company.create(
	id: 1,
	name: 'Company 1',
	description: 'Desc 1',
	address: 'SAADSA',
	email: 'adv@voove.ru')
Company.create(
	id: 2,
	name: 'Company 2',
	description: 'DSadada',
	address: 'asdasdsasda',
	email: 'asdad@example.com')
Review.create(
	id: 1,
	user_id: 3,
	item_id: 1,
	created_at: Time.now,
	mark: 5,
	text: 'Cool')
Review.create(
	id: 2,
	user_id: 2,
	item_id: 1,
	created_at: Time.now,
	mark: 4,
	text: 'no Cool')
Review.create(
	id: 3,
	user_id: 3,
	item_id: 2,
	created_at: Time.now,
	mark: 4,
	text: 'dsffsf Cool')
Review.create(
	id: 4,
	user_id: 3,
	item_id: 3,
	created_at: Time.now,
	mark: 5,
	text: 'Cool sddaadsad!')
Review.create(
	id: 5,
	user_id: 1,
	item_id: 2,
	created_at: Time.now,
	mark: 3,
	text: 'NOsdadadaddCool')
Review.create(
	id: 6,
	user_id: 2,
	item_id: 3,
	created_at: Time.now,
	mark: 5,
	text: 'Cool')
Comment.create(
	id: 1,
	user_id: 2,
	item_id: 1,
	created_at: Time.now,
	text: 'asdasdasda')
Comment.create(
	id: 2,
	user_id: 3,
	item_id: 2,
	created_at: Time.now,
	text: 'asdsadasdaasdasda')
Comment.create(
	id: 3,
	user_id: 3,
	item_id: 2,
	created_at: Time.now,
	text: 'asdasaSsasdasda')
Comment.create(
	id: 4,
	user_id: 2,
	item_id: 3,
	created_at: Time.now,
	text: 'asdasdasdasdasda')
Sales.create(
	id: 1,
	user_id: 2,
	item_id: 1,
	created_at: Time.now)
Sales.create(
	id: 2,
	user_id: 3,
	item_id: 2,
	created_at: Time.now)

DataMapper.finalize
