require 'sinatra/base'
require './app/helpers.rb'
require './app/modeles.rb'

class App < Sinatra::Base
	include Helper

	enable :sessions

	before do
		@title 	= 'Ваш купон'
		@user 	=  User.first(id: 1)#session[:uid])
	end

	before '/admin/*' do
		begin 
			protected!
		rescue => e 
			redirect '/'
		end
	end

	get '/admin' do
		erb :admin, locals: { users: User.all, faq: Faq.all, coupons: Coupon.all }
	end

	get '/' do
		erb :main, locals: { coupons: Coupon.all }
	end

	get '/about' do 
		erb :about
	end

	#
	# Users
	#
	get '/users' do
		erb :users, locals: { users: User.all } 
	end

	get '/users/:id' do 
		user = User.first(id: params[:id].to_i)
		not_found 'Страница не найдена' if user.nil?
		erb :user, locals: { user: user }
	end

	post '/users' do
		# raise Exception.new unless params[:password].equal? params[:repass]
		user = User.create({
			login: 		params[:login],
			email: 		params[:email],
			password: 	crypt(params[:password]),
			created_at: Time.now,
			updated_at: Time.now,
			rights: 	0b001
			}) 
		redirect '/'
	end

	put '/users/:id' do
		user = User.first(id: params[:id].to_i)
		not_found 'Страница не найдена' if user.nil?
		user.login 		= params[:login]
		user.email 		= params[:email]
		user.rights 	= params[:rights] if params.keys.include?(:rights)
		user.save
	end

	get '/admin/users' do
		erb :users, locals: { users: User.all }
	end

	delete '/users/:id' do
		User.first(id: params[:id].to_i).destroy
	end
	
	get '/login' do
		erb :login
	end

	get '/signin' do
		erb :signin
	end

	post '/signin' do
		begin
			user = User.first(login: params[:login], password: crypt(params[:password]))
			raise Exception.new if user.nil?
			session[:uid] = user.id;
			redirect '/'
		rescue => e 
			puts "Fail! Signin: #{e.message}"
		end
	end

	get '/signup' do 
		erb :signup
	end

	get '/signout' do
		session[:uid] = ''
		redirect '/'
	end
	#
	# Coupon
	#
	get '/coupons' do
		erb :main, locals: { coupons: Coupon.all }
	end

	post '/coupons' do
		redirect('/') unless protected?
		Coupon.create({
			name: params[:name]
			})
	end

	get '/coupons/:id' do
		iid = params[:id].to_i
		erb :coupon, locals: { coupon: Coupon.first(id: iid), 
			likes: getLikes(iid), 
			comments: getComments(iid),
			reviews: getReviews(iid) }
	end

	put '/coupons/:id' do
		redirect back if protected?
		coupon = Coupon.first(id: params[:id])
		# TODO
		coupon.save
	end

	delete '/coupons/:id' do
		Coupon.first(id: params[:id].to_i).destroy
		redirect back
	end

	#
	# Comments
	#
	post '/comments' do
		args = params
		args[:user_id] = @user.id
		Comment.create args
	end

	#
	# Review
	#
	post '/reviews' do
		args = params
		args[:user_id] = @user.id
		review = Review.first(item_id: args[:item_id], user_id: @user_id)
		if review.nil?
			Review.create args
		else
			review.mark = params[:mark].to_i
			review.text = params[:text]
			review.created_at = Time.now
			review.save
		end
	end

	#
	# Like
	#
	post '/likes' do
		like = Like.first(id: @user.id, item_id: params[:item_id].to_i)
		if like.nil?
			Like.create id: @user.id, item_id: params[:item_id].to_i
		else
			like.destroy
		end
	end

	#
	# Sales
	#
	post '/sales' do
		Sales.create user_id: @user.id, item_id: params[:item_id].to_i, created_at: Time.now
	end

	#
	# FAQ
	#
	get '/faq' do 
		erb :faq, locals: { faq: Faq.all } 
	end

	get '/admin/faq/new' do
		erb :faq_new
	end

	post '/admin/faq' do
		Faq.create(params)
		redirect('/faq')
	end
end